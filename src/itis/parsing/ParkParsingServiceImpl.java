package itis.parsing;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ParkParsingServiceImpl implements ParkParsingService {

    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {

        Map<String,String> stringHashMap= new HashMap<>();

        try {
            Constructor<Park> constructor = Park.class.getDeclaredConstructor();
            constructor.setAccessible(true);
            Park park = constructor.newInstance();

            List<ParkParsingException.ParkValidationError> parkValidationErrors= new ArrayList<>();
            BufferedReader reader = new BufferedReader(new FileReader(parkDatafilePath));
            String line="";
            while ((line = reader.readLine()) != null){
                if (line.equals("***")) continue;
                String[] split = line.split(":");
                String substring = split[0].substring(1,split[0].length()-2);
                String s = split[1];
                if (split[1].charAt(0)=='"'){
                    s = split[1].substring(1,split[1].length()-2);
                }
                stringHashMap.put(substring,s);
            }
            Field[] declaredFields = Park.class.getDeclaredFields();
            for (String name: stringHashMap.keySet()){
                for (Field field: declaredFields){

                    if (name.equals(field.getName())){
                        field.setAccessible(true);
                        field.set(park,stringHashMap.get(name));

                        if (field.isAnnotationPresent(FieldName.class)){
                            String srcFieldName = field.getAnnotation(FieldName.class).value();

                            Field srcField = null;
                            try {
                                srcField = park.getClass().getField(srcFieldName);
                                field.set(park,srcField.get(park));

                            } catch (NoSuchFieldException | IllegalAccessException e) {
                                parkValidationErrors.add(
                                        new ParkParsingException.ParkValidationError
                                                (name,"\n" +
                                                        "For the field"+name+"source field not found "+srcFieldName));
                                if (field.isAnnotationPresent(MaxLength.class)
                                        && field.getType()==String.class
                                        && stringHashMap.get(name).length() > field.getAnnotation(MaxLength.class).value())
                                {
                                    parkValidationErrors.add(
                                            new ParkParsingException.ParkValidationError
                                                    (name, "Field length" + name + "\n" +
                                                            "should be no more " + field.getAnnotation(MaxLength.class).value()));
                                }
                            }


                        }
                        {
                            parkValidationErrors.add(
                                    new ParkParsingException.ParkValidationError
                                            (name,"Field"+name+" cannot be empty\n" ));
                        }
                        if (field.isAnnotationPresent(NotBlank.class)
                                && (stringHashMap.get(name)!=null || !stringHashMap.get(name).equals("")))
                        if (!parkValidationErrors.isEmpty()) {
                            throw new ParkParsingException("\n" +
                                    "Errors occurred while parsing",parkValidationErrors);
                        }

                    }
                }
            }
            reader.close();
            return park;
        } catch (IOException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | InstantiationException e) {
            System.out.println("Ошибка:" + e);
        }
        return null;
    }
}
